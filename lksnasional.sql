-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2018 at 02:43 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lksnasional`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_orders`
--

CREATE TABLE `detail_orders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `messeges` varchar(198) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_orders`
--

INSERT INTO `detail_orders` (`id`, `order_id`, `menu_id`, `qty`, `price`, `messeges`) VALUES
(3, 26, 4, 1, 10000, 'Nothing'),
(4, 26, 1, 1, 18000, 'Nothing'),
(5, 27, 4, 1, 10000, 'Nothing'),
(6, 27, 1, 1, 18000, 'Nothing'),
(7, 27, 8, 1, 33000, 'Nothing'),
(8, 28, 4, 1, 10000, 'Nothing'),
(9, 28, 1, 1, 18000, 'Nothing'),
(10, 29, 6, 1, 17000, 'Nothing'),
(11, 29, 1, 1, 18000, 'Nothing'),
(12, 29, 7, 1, 25000, 'Nothing'),
(13, 30, 4, 1, 10000, 'Nothing'),
(14, 30, 1, 1, 18000, 'Nothing');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `photo` varchar(198) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `code`, `name`, `price`, `photo`) VALUES
(1, 'N88578899', 'Mie Ayam Kamu', 18000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\003-chinese-food.png'),
(2, 'UUDHHH', 'Burger Up', 23000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\007-burger-1.png'),
(3, 'BG75786', 'Burger Alone', 20000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\010-burge'),
(4, 'COOLR34', 'Cola Run', 10000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\012-cola.png'),
(5, 'TE334', 'TEA Hai', 6000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\002-tea.png'),
(6, 'K3TTY', 'Quuick Nice', 17000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\008-fried-potatoes.png'),
(7, 'CPP200', 'COFFE HOT', 25000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\040-coffee-cup.png'),
(8, 'CIISU99', 'COFFE AHH', 33000, 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\009-coffee.png');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `employed_id` int(11) NOT NULL,
  `promo_id` int(11) DEFAULT NULL,
  `code_transaksi` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `payment_type` enum('CASH','CREDIT','D-CASH') NOT NULL,
  `total_payment` int(11) NOT NULL,
  `card_number` varchar(50) DEFAULT NULL,
  `bank` varchar(50) DEFAULT NULL,
  `dibayar` int(11) DEFAULT NULL,
  `kembalian` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `employed_id`, `promo_id`, `code_transaksi`, `date`, `payment_type`, `total_payment`, `card_number`, `bank`, `dibayar`, `kembalian`) VALUES
(26, 3, 2, 12, 'REST-NT-20181021-0730-21', '2018-10-22', 'CREDIT', 28000, '', '', 30000, 2000),
(27, NULL, 2, NULL, 'REST-NT-20181021-0730-22', '2018-10-21', 'CASH', 61000, '', '', 70000, 9000),
(28, NULL, 2, NULL, 'REST-NT-20181021-0750-33', '2018-10-21', 'CASH', 28000, '', '', 50000, 22000),
(29, NULL, 2, NULL, 'REST-NT-20181021-0926-52', '2018-10-21', 'CASH', 60000, '', '', 100000, 40000),
(30, NULL, 2, NULL, 'REST-NT-20181021-1001-02', '2018-10-21', 'CASH', 28000, '', '', 30000, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` enum('Disc','Potongan','Reward') NOT NULL,
  `promo` varchar(50) NOT NULL,
  `status` enum('Actived','Expired') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo`
--

INSERT INTO `promo` (`id`, `code`, `name`, `type`, `promo`, `status`) VALUES
(7, '25OKTOBER', '100% FREE', 'Disc', '90', 'Actived'),
(8, 'GRATISMAKAN', 'Dapat Makan Gratis', 'Potongan', '15000', 'Actived'),
(9, 'AKHIRTAHUN', 'Gratis Makan Malam di Tahun Baru', 'Disc', '15', 'Expired'),
(10, 'JATENGGAYENG', 'JATENG GAYENG', 'Potongan', '4000', 'Actived'),
(11, 'COBACOBA', 'Gratis Coffee', 'Reward', 'Gratis Hot Coffe (1)', 'Actived'),
(12, 'COBACOBA2', 'Gratis Coffee', 'Reward', 'Gratis Ice Coffe (1)', 'Actived');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'member'),
(3, 'cashier'),
(4, 'chef');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `brithday` date NOT NULL,
  `joindate` date NOT NULL,
  `photo` varchar(198) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `hp`, `brithday`, `joindate`, `photo`, `role_id`) VALUES
(1, 'Dawam Raja', 'dawam@dev.id', 'ac43724f16e9241d990427ab7c8f4228', '08847758867', '2000-12-14', '2018-10-20', 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\girl-6.png', 1),
(2, 'Kasir Satus', 'kasir11@gmail.com', 'ac43724f16e9241d990427ab7c8f4228', '048857738889', '2002-10-21', '2018-10-20', 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\boy-10.png', 3),
(3, 'Member Satu', 'member1@gmail.com', 'ac43724f16e9241d990427ab7c8f4228', '00348847727', '2018-10-20', '2018-10-20', 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\boy-11.png', 2),
(8, 'Chef Satu', 'chef1@gmail.com', 'ac43724f16e9241d990427ab7c8f4228', '04985678999', '2018-10-20', '2018-10-20', 'D:\\PROJECT\\Restorant\\Restorant\\Resources\\boy-9.png', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_orders`
--
ALTER TABLE `detail_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id_menu_id` (`order_id`,`menu_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_transaksi` (`code_transaksi`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `employed_id` (`employed_id`),
  ADD KEY `promo_id` (`promo_id`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_orders`
--
ALTER TABLE `detail_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_orders`
--
ALTER TABLE `detail_orders`
  ADD CONSTRAINT `detail_orders_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`),
  ADD CONSTRAINT `detail_orders_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`employed_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`promo_id`) REFERENCES `promo` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
