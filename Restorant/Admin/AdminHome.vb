﻿Imports MySql.Data.MySqlClient
Public Class AdminHome
    Private Sub AdminHome_Load(sender As Object, e As EventArgs) Handles Me.Load
        namaAdmin.Text = nameUser
        PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        PictureBox1.Image = Image.FromFile(photoUser)
    End Sub
    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btAccount_Click(sender As Object, e As EventArgs) Handles btAccount.Click
        AdminAccount.Show()
        Me.Hide()
    End Sub
End Class
