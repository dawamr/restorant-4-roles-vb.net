﻿Imports MySql.Data.MySqlClient
Public Class AdminMenu
    Private Sub fromAdmin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Menu1()
        loadData()
        txtDisabled()
    End Sub

    Sub txtDisabled()
        txtName.Enabled = False
        txtCode.Enabled = False
        txtPrice.Enabled = False
        byName.Checked = True
        txtkosong()
        btDelete.Enabled = True
        btSave.Enabled = False
    End Sub
    Sub txtEnabled()
        txtName.Enabled = True
        txtPrice.Enabled = True
        btSave.Enabled = True
        btDelete.Enabled = False
    End Sub
    Sub txtkosong()
        txtName.Text = ""
        txtPrice.Text = ""
        txtCode.Text = ""
    End Sub

    Sub Menu1()
        btHome.ForeColor = Color.Silver
        btMenu.ForeColor = Color.White
        btEmployee.ForeColor = Color.Silver
        btAccount.ForeColor = Color.Silver
        btMember.ForeColor = Color.Silver
        btLogout.ForeColor = Color.Red

    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Sub loadData()
        Try
            Call koneksi()
            str = "select name, price,code from menu  order by name asc"
            dt = New DataTable
            da = New MySql.Data.MySqlClient.MySqlDataAdapter(str, conn)
            DBData = New MySql.Data.MySqlClient.MySqlCommandBuilder(da) 'untuk bisa edit datagridview
            da.Fill(dt)
            DataGridView1.DataSource = dt

            DataGridView1.Columns(0).HeaderText = "Menu"
            DataGridView1.Columns(1).HeaderText = "Harga"

            'DGView(Properties)
            DataGridView1.DefaultCellStyle.Font = New Font("Candara", 14)
            DataGridView1.RowHeadersDefaultCellStyle.Font = New Font("Candara", 14, FontStyle.Bold)
            DataGridView1.ReadOnly = True
            DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders

            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub dataKlik()
        Try
            If DataGridView1.RowCount > 0 Then
                With DataGridView1
                    baris = .CurrentRow.Index
                    txtCode.Text = CStr(.Item(2, baris).Value)
                    txtName.Text = CStr(.Item(0, baris).Value)
                    txtPrice.Text = CStr(.Item(1, baris).Value)
                    Call koneksi()
                    str = "select photo from menu where code = '" & CStr(.Item(2, baris).Value) & "' limit 1"
                    cmd = New MySqlCommand(str, conn)
                    rd = cmd.ExecuteReader
                    rd.Read()
                    If rd.HasRows Then
                        path = CStr(rd("photo"))
                        pictMenu1.SizeMode = PictureBoxSizeMode.StretchImage
                        pictMenu1.Image = Image.FromFile(path)
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        dataKlik()
    End Sub
    Sub autoComplateName()
        Call koneksi()
        str = "select name, code from menu order by name asc"
        cmd = New MySqlCommand(str, conn)
        rd = cmd.ExecuteReader

        If rd.HasRows Then
            Do While rd.Read
                QuerySearch.AutoCompleteCustomSource.Add(CStr(rd("name")))
            Loop

        End If
    End Sub
    Sub autoComplateCode()
        Call koneksi()
        str = "select  code from menu order by name asc"
        cmd = New MySqlCommand(str, conn)
        rd = cmd.ExecuteReader

        If rd.HasRows Then
            Do While rd.Read
                QuerySearch.AutoCompleteCustomSource.Add(CStr(rd("code")))
            Loop

        End If
    End Sub
    Private Sub btUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btUpload.Click
        On Error Resume Next
        OpenFileDialog1.Filter = "JPG FILES(*jpg)|*.jpg|JPEG FILEA(*jpeg)|*.jpeg|PNG FILES (*png)|*.png"
        OpenFileDialog1.FileName = ""
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            pictMenu1.SizeMode = PictureBoxSizeMode.StretchImage
            pictMenu1.Image = New Bitmap(OpenFileDialog1.FileName)
            path = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub btNew_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btNew.Click
        txtEnabled()
        txtkosong()
        txtCode.Enabled = True
        btNew.Visible = False
        DataGridView1.Enabled = False
        btEdit.Visible = False
        btCancel.Visible = True
    End Sub
    Private Sub btEdit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEdit.Click
        txtEnabled()
        btNew.Visible = False
        btEdit.Visible = False
        btCancel.Visible = True
        txtCode.Enabled = False
        DataGridView1.Enabled = True
    End Sub
    Private Sub btSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSave.Click
        Try
            Call koneksi()
            str = "select code from menu where code = '" & txtCode.Text & "'"
            cmd = New MySqlCommand(str, conn)
            rd = cmd.ExecuteReader
            If rd.HasRows Then
                Call koneksi()
                str = "update menu set name = '" & txtName.Text & "', price = '" & txtPrice.Text & "', photo = '" & Replace(path, "\", "\\") & "' where code = '" & txtCode.Text & "'"
                cmd = New MySqlCommand(str, conn)
                cmd.ExecuteNonQuery()
                loadData()
                MsgBox("Berhasil Mengubah Data Menu")
            Else
                Call koneksi()
                str = "insert into menu (code,name,price,photo) VALUES ('" & txtCode.Text & "','" & txtName.Text & "','" & txtPrice.Text & "','" & Replace(path, "\", "\\") & "')"
                cmd = New MySqlCommand(str, conn)
                cmd.ExecuteNonQuery()
                loadData()
                MsgBox("Berhail Menambah Menu Baru", vbInformation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btCancel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCancel.Click
        txtDisabled()
        btCancel.Visible = False
        btNew.Visible = True
        btEdit.Visible = True
        DataGridView1.Enabled = True
    End Sub
    Private Sub btDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btDelete.Click
        Try
            Call koneksi()
            str = "select code from menu where code = '" & txtCode.Text & "'"
            cmd = New MySqlCommand(str, conn)
            rd = cmd.ExecuteReader
            If rd.HasRows Then
                Call koneksi()
                str = "delete from menu where code = '" & txtCode.Text & "'"
                cmd = New MySqlCommand(str, conn)
                cmd.ExecuteNonQuery()
                loadData()
                MsgBox("Berhasil Di Hapus !", vbInformation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub QuerySearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles QuerySearch.TextChanged
        If byName.Checked Then
            QuerySearch.AutoCompleteCustomSource.Clear()
            autoComplateName()
            Try
                Call koneksi()
                str = "select name, price,code from menu where name like '%" & QuerySearch.Text & "%' order by name asc"
                cmd = New MySqlCommand(str, conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    Call koneksi()
                    da = New MySql.Data.MySqlClient.MySqlDataAdapter("select name, price,code from menu where name like '%" & QuerySearch.Text & "%' order by name asc", conn)
                    ds = New DataSet
                    da.Fill(ds, "ketemu")
                    DataGridView1.DataSource = ds.Tables("ketemu")
                    DataGridView1.ReadOnly = True
                Else
                    MsgBox("Tidak Di Temukan !", vbInformation)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        If byCode.Checked Then
            QuerySearch.AutoCompleteCustomSource.Clear()
            autoComplateCode()
            Try
                Call koneksi()
                str = "select name, price,code from menu where code like '%" & QuerySearch.Text & "%' order by name asc"
                cmd = New MySqlCommand(str, conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    Call koneksi()
                    da = New MySql.Data.MySqlClient.MySqlDataAdapter("select code, price,name from menu where code like '%" & QuerySearch.Text & "%' order by name asc", conn)
                    ds = New DataSet
                    da.Fill(ds, "ketemu")
                    DataGridView1.DataSource = ds.Tables("ketemu")
                    DataGridView1.ReadOnly = True
                Else
                    MsgBox("Tidak Di Temukan !", vbInformation)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

        End If
    End Sub

    Private Sub btHome_Click(sender As Object, e As EventArgs) Handles btHome.Click
        AdminHome.Show()
        Me.Hide()
    End Sub

    Private Sub btEmployee_Click(sender As Object, e As EventArgs) Handles btEmployee.Click
        AdminEmployee.Show()
        Me.Hide()
    End Sub

    Private Sub btPromo_Click(sender As Object, e As EventArgs)
        AdminPromo.Show()
        Me.Hide()
    End Sub
End Class
