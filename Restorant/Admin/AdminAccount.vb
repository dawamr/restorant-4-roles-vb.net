﻿Imports MySql.Data.MySqlClient
Public Class AdminAccount
    Private Sub fromAdmin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        loadData()
    End Sub

    Sub loadData()
        Try
            namaAdmin.Text = nameUser
            namaAdmin2.Text = nameUser
            txtName.Text = nameUser
            txtEmail.Text = emailUser
            txtHp.Text = hpUser
            txtDate.Text = Format(CDate(brithdayUser), "yyyy-MM-dd")
            txtJoinDate.Text = Format(CDate(joinDate), "yyyy-MM-dd")
            PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
            PictureBox1.Image = Image.FromFile(photoUser)
            pict2.SizeMode = PictureBoxSizeMode.StretchImage
            pict2.Image = Image.FromFile(photoUser)
            pict3.SizeMode = PictureBoxSizeMode.StretchImage
            pict3.Image = Image.FromFile(photoUser)
            btCancel.Visible = False
            btSave.Visible = False
            btEdit.Visible = True
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Sub txtEnabled()
        txtName.Enabled = True
        txtEmail.Enabled = True
        txtHp.Enabled = True
        txtDate.Enabled = True
        btUpload.Enabled = True
    End Sub
    Sub txtDisabled()
        txtName.Enabled = False
        txtEmail.Enabled = False
        txtHp.Enabled = False
        txtDate.Enabled = False
        btUpload.Enabled = False
    End Sub
    Private Sub btEdit_Click(sender As Object, e As EventArgs) Handles btEdit.Click
        btCancel.Visible = True
        btSave.Visible = True
        btEdit.Visible = False
        txtEnabled()
    End Sub

    Private Sub btUpload_Click(sender As Object, e As EventArgs) Handles btUpload.Click
        On Error Resume Next
        OpenFileDialog1.Filter = "FILE PNG|*.png|FILE JPG|*.jpg"
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            pict3.SizeMode = PictureBoxSizeMode.StretchImage
            path = OpenFileDialog1.FileName
            pict3.Image = New Bitmap(path)
            txtPhoto.Text = path.Substring(path.LastIndexOf("\") + 1)
            'Change Name And copy file if file name > 30 character
            'If path.Count > 30 Then
            '    txtPhoto.Text = "IMG-" + Format(Date.Now(), "yyyy_MM_dd-hh_mm")
            'Else
            '    txtPhoto.Text = path.Substring(path.LastIndexOf("\") + 1)
            'End If
        End If
    End Sub

    Private Sub btCancel_Click(sender As Object, e As EventArgs) Handles btCancel.Click
        txtDisabled()
        btCancel.Visible = False
        btSave.Visible = False
        btEdit.Visible = True
        loadData()
    End Sub

    Private Sub btSave_Click(sender As Object, e As EventArgs) Handles btSave.Click
        Try
            nameUser = txtName.Text
            emailUser = txtEmail.Text
            hpUser = txtHp.Text
            brithdayUser = Format(CDate(txtDate.Text), "yyyy-MM-dd")
            photoUser = path.Replace("\", "\\")
            UpdateProfile()
            loadData()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bg1_Click(sender As Object, e As EventArgs) Handles bg1.Click
        bg.BackColor = bg1.BackColor
    End Sub

    Private Sub bg2_Click(sender As Object, e As EventArgs) Handles bg2.Click
        bg.BackColor = bg2.BackColor
    End Sub

    Private Sub bg3_Click(sender As Object, e As EventArgs) Handles bg3.Click
        bg.BackColor = bg3.BackColor
    End Sub

    Private Sub bg4_Click(sender As Object, e As EventArgs) Handles bg4.Click
        bg.BackColor = bg4.BackColor
    End Sub

    Private Sub bg_Paint(sender As Object, e As PaintEventArgs) Handles bg.Paint

    End Sub
End Class
