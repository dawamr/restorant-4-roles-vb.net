﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btPromo = New System.Windows.Forms.Button()
        Me.PanelLogout = New System.Windows.Forms.Panel()
        Me.btLogout = New System.Windows.Forms.Button()
        Me.PanelPassword = New System.Windows.Forms.Panel()
        Me.btAccount = New System.Windows.Forms.Button()
        Me.PanelMember = New System.Windows.Forms.Panel()
        Me.btMember = New System.Windows.Forms.Button()
        Me.PanelMenu = New System.Windows.Forms.Panel()
        Me.btMenu = New System.Windows.Forms.Button()
        Me.PanelEmployee = New System.Windows.Forms.Panel()
        Me.btEmployee = New System.Windows.Forms.Button()
        Me.PanelHome = New System.Windows.Forms.Panel()
        Me.btHome = New System.Windows.Forms.Button()
        Me.namaAdmin = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PanelEmployee2 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.byCode = New System.Windows.Forms.RadioButton()
        Me.byName = New System.Windows.Forms.RadioButton()
        Me.QuerySearch = New System.Windows.Forms.TextBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.btNew = New System.Windows.Forms.Button()
        Me.btCancel = New System.Windows.Forms.Button()
        Me.btDelete = New System.Windows.Forms.Button()
        Me.btEdit = New System.Windows.Forms.Button()
        Me.btSave = New System.Windows.Forms.Button()
        Me.btUpload = New System.Windows.Forms.Button()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtCode = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pictMenu1 = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.PanelLogout.SuspendLayout()
        Me.PanelPassword.SuspendLayout()
        Me.PanelMember.SuspendLayout()
        Me.PanelMenu.SuspendLayout()
        Me.PanelEmployee.SuspendLayout()
        Me.PanelHome.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelEmployee2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel9.SuspendLayout()
        CType(Me.pictMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.namaAdmin)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(207, 536)
        Me.Panel1.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.PanelLogout)
        Me.Panel3.Controls.Add(Me.PanelPassword)
        Me.Panel3.Controls.Add(Me.PanelMember)
        Me.Panel3.Controls.Add(Me.PanelMenu)
        Me.Panel3.Controls.Add(Me.PanelEmployee)
        Me.Panel3.Controls.Add(Me.PanelHome)
        Me.Panel3.Location = New System.Drawing.Point(0, 145)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(207, 351)
        Me.Panel3.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.Panel4.Controls.Add(Me.btPromo)
        Me.Panel4.Location = New System.Drawing.Point(0, 156)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(207, 39)
        Me.Panel4.TabIndex = 18
        '
        'btPromo
        '
        Me.btPromo.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btPromo.FlatAppearance.BorderSize = 0
        Me.btPromo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btPromo.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btPromo.Location = New System.Drawing.Point(4, 0)
        Me.btPromo.Name = "btPromo"
        Me.btPromo.Size = New System.Drawing.Size(203, 39)
        Me.btPromo.TabIndex = 4
        Me.btPromo.Text = "Manage Promo"
        Me.btPromo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btPromo.UseVisualStyleBackColor = False
        '
        'PanelLogout
        '
        Me.PanelLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelLogout.Controls.Add(Me.btLogout)
        Me.PanelLogout.Location = New System.Drawing.Point(0, 297)
        Me.PanelLogout.Name = "PanelLogout"
        Me.PanelLogout.Size = New System.Drawing.Size(207, 39)
        Me.PanelLogout.TabIndex = 21
        '
        'btLogout
        '
        Me.btLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btLogout.FlatAppearance.BorderSize = 0
        Me.btLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btLogout.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btLogout.Location = New System.Drawing.Point(4, 0)
        Me.btLogout.Name = "btLogout"
        Me.btLogout.Size = New System.Drawing.Size(203, 39)
        Me.btLogout.TabIndex = 7
        Me.btLogout.Text = "Logout"
        Me.btLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btLogout.UseVisualStyleBackColor = False
        '
        'PanelPassword
        '
        Me.PanelPassword.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelPassword.Controls.Add(Me.btAccount)
        Me.PanelPassword.Location = New System.Drawing.Point(0, 250)
        Me.PanelPassword.Name = "PanelPassword"
        Me.PanelPassword.Size = New System.Drawing.Size(207, 39)
        Me.PanelPassword.TabIndex = 20
        '
        'btAccount
        '
        Me.btAccount.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btAccount.FlatAppearance.BorderSize = 0
        Me.btAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btAccount.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btAccount.Location = New System.Drawing.Point(4, 0)
        Me.btAccount.Name = "btAccount"
        Me.btAccount.Size = New System.Drawing.Size(203, 39)
        Me.btAccount.TabIndex = 7
        Me.btAccount.Text = "Account Detail"
        Me.btAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btAccount.UseVisualStyleBackColor = False
        '
        'PanelMember
        '
        Me.PanelMember.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelMember.Controls.Add(Me.btMember)
        Me.PanelMember.Location = New System.Drawing.Point(0, 203)
        Me.PanelMember.Name = "PanelMember"
        Me.PanelMember.Size = New System.Drawing.Size(207, 39)
        Me.PanelMember.TabIndex = 19
        '
        'btMember
        '
        Me.btMember.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btMember.FlatAppearance.BorderSize = 0
        Me.btMember.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btMember.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMember.Location = New System.Drawing.Point(4, 0)
        Me.btMember.Name = "btMember"
        Me.btMember.Size = New System.Drawing.Size(203, 39)
        Me.btMember.TabIndex = 5
        Me.btMember.Text = "Manage Member"
        Me.btMember.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btMember.UseVisualStyleBackColor = False
        '
        'PanelMenu
        '
        Me.PanelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.PanelMenu.Controls.Add(Me.btMenu)
        Me.PanelMenu.Location = New System.Drawing.Point(0, 109)
        Me.PanelMenu.Name = "PanelMenu"
        Me.PanelMenu.Size = New System.Drawing.Size(207, 39)
        Me.PanelMenu.TabIndex = 17
        '
        'btMenu
        '
        Me.btMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btMenu.FlatAppearance.BorderSize = 0
        Me.btMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btMenu.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMenu.Location = New System.Drawing.Point(4, 0)
        Me.btMenu.Name = "btMenu"
        Me.btMenu.Size = New System.Drawing.Size(203, 39)
        Me.btMenu.TabIndex = 4
        Me.btMenu.Text = "Manage Menu"
        Me.btMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btMenu.UseVisualStyleBackColor = False
        '
        'PanelEmployee
        '
        Me.PanelEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelEmployee.Controls.Add(Me.btEmployee)
        Me.PanelEmployee.Location = New System.Drawing.Point(0, 62)
        Me.PanelEmployee.Name = "PanelEmployee"
        Me.PanelEmployee.Size = New System.Drawing.Size(207, 39)
        Me.PanelEmployee.TabIndex = 16
        '
        'btEmployee
        '
        Me.btEmployee.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btEmployee.FlatAppearance.BorderSize = 0
        Me.btEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btEmployee.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEmployee.Location = New System.Drawing.Point(4, 0)
        Me.btEmployee.Name = "btEmployee"
        Me.btEmployee.Size = New System.Drawing.Size(203, 39)
        Me.btEmployee.TabIndex = 3
        Me.btEmployee.Text = "Manage Employee"
        Me.btEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btEmployee.UseVisualStyleBackColor = False
        '
        'PanelHome
        '
        Me.PanelHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelHome.Controls.Add(Me.btHome)
        Me.PanelHome.Location = New System.Drawing.Point(0, 15)
        Me.PanelHome.Name = "PanelHome"
        Me.PanelHome.Size = New System.Drawing.Size(207, 39)
        Me.PanelHome.TabIndex = 15
        '
        'btHome
        '
        Me.btHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.btHome.FlatAppearance.BorderSize = 0
        Me.btHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btHome.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btHome.Location = New System.Drawing.Point(4, 0)
        Me.btHome.Name = "btHome"
        Me.btHome.Size = New System.Drawing.Size(203, 39)
        Me.btHome.TabIndex = 2
        Me.btHome.Text = "Home"
        Me.btHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btHome.UseVisualStyleBackColor = False
        '
        'namaAdmin
        '
        Me.namaAdmin.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.namaAdmin.Location = New System.Drawing.Point(0, 117)
        Me.namaAdmin.Name = "namaAdmin"
        Me.namaAdmin.Size = New System.Drawing.Size(207, 25)
        Me.namaAdmin.TabIndex = 1
        Me.namaAdmin.Text = "Nama"
        Me.namaAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Restorant.My.Resources.Resources.boy_8
        Me.PictureBox1.Location = New System.Drawing.Point(58, 24)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(90, 90)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PictureBox4)
        Me.Panel2.Controls.Add(Me.PictureBox3)
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(207, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(810, 15)
        Me.Panel2.TabIndex = 1
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.Restorant.My.Resources.Resources.transparent_button_yellow_3
        Me.PictureBox4.Location = New System.Drawing.Point(774, 2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 2
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.Restorant.My.Resources.Resources.green_dog_bone_png
        Me.PictureBox3.Location = New System.Drawing.Point(756, 2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 1
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.Restorant.My.Resources.Resources.button_svg_2
        Me.PictureBox2.Location = New System.Drawing.Point(792, 2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(312, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(145, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Manage Menu"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.GridColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView1.Location = New System.Drawing.Point(19, 75)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(263, 434)
        Me.DataGridView1.TabIndex = 1
        '
        'PanelEmployee2
        '
        Me.PanelEmployee2.BackColor = System.Drawing.Color.White
        Me.PanelEmployee2.Controls.Add(Me.GroupBox1)
        Me.PanelEmployee2.Controls.Add(Me.Panel9)
        Me.PanelEmployee2.Controls.Add(Me.btUpload)
        Me.PanelEmployee2.Controls.Add(Me.txtPrice)
        Me.PanelEmployee2.Controls.Add(Me.txtName)
        Me.PanelEmployee2.Controls.Add(Me.txtCode)
        Me.PanelEmployee2.Controls.Add(Me.Label6)
        Me.PanelEmployee2.Controls.Add(Me.Label7)
        Me.PanelEmployee2.Controls.Add(Me.Label8)
        Me.PanelEmployee2.Controls.Add(Me.Label5)
        Me.PanelEmployee2.Controls.Add(Me.Label4)
        Me.PanelEmployee2.Controls.Add(Me.Label3)
        Me.PanelEmployee2.Controls.Add(Me.Label2)
        Me.PanelEmployee2.Controls.Add(Me.pictMenu1)
        Me.PanelEmployee2.Controls.Add(Me.DataGridView1)
        Me.PanelEmployee2.Controls.Add(Me.Label1)
        Me.PanelEmployee2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelEmployee2.Location = New System.Drawing.Point(207, 15)
        Me.PanelEmployee2.Name = "PanelEmployee2"
        Me.PanelEmployee2.Size = New System.Drawing.Size(810, 521)
        Me.PanelEmployee2.TabIndex = 3
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.byCode)
        Me.GroupBox1.Controls.Add(Me.byName)
        Me.GroupBox1.Controls.Add(Me.QuerySearch)
        Me.GroupBox1.Location = New System.Drawing.Point(302, 346)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(484, 112)
        Me.GroupBox1.TabIndex = 59
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Search Data"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(42, 33)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 19)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Search By"
        '
        'byCode
        '
        Me.byCode.AutoSize = True
        Me.byCode.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.byCode.Location = New System.Drawing.Point(236, 30)
        Me.byCode.Name = "byCode"
        Me.byCode.Size = New System.Drawing.Size(66, 23)
        Me.byCode.TabIndex = 2
        Me.byCode.TabStop = True
        Me.byCode.Text = "Code"
        Me.byCode.UseVisualStyleBackColor = True
        '
        'byName
        '
        Me.byName.AutoSize = True
        Me.byName.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.byName.Location = New System.Drawing.Point(141, 30)
        Me.byName.Name = "byName"
        Me.byName.Size = New System.Drawing.Size(69, 23)
        Me.byName.TabIndex = 1
        Me.byName.TabStop = True
        Me.byName.Text = "Name"
        Me.byName.UseVisualStyleBackColor = True
        '
        'QuerySearch
        '
        Me.QuerySearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.QuerySearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.QuerySearch.Location = New System.Drawing.Point(46, 71)
        Me.QuerySearch.Name = "QuerySearch"
        Me.QuerySearch.Size = New System.Drawing.Size(425, 27)
        Me.QuerySearch.TabIndex = 0
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.btNew)
        Me.Panel9.Controls.Add(Me.btCancel)
        Me.Panel9.Controls.Add(Me.btDelete)
        Me.Panel9.Controls.Add(Me.btEdit)
        Me.Panel9.Controls.Add(Me.btSave)
        Me.Panel9.Location = New System.Drawing.Point(490, 272)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(296, 45)
        Me.Panel9.TabIndex = 55
        '
        'btNew
        '
        Me.btNew.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btNew.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btNew.ForeColor = System.Drawing.Color.White
        Me.btNew.Location = New System.Drawing.Point(13, 8)
        Me.btNew.Name = "btNew"
        Me.btNew.Size = New System.Drawing.Size(85, 29)
        Me.btNew.TabIndex = 32
        Me.btNew.Text = "New"
        Me.btNew.UseVisualStyleBackColor = False
        '
        'btCancel
        '
        Me.btCancel.BackColor = System.Drawing.Color.Orange
        Me.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btCancel.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btCancel.ForeColor = System.Drawing.Color.White
        Me.btCancel.Location = New System.Drawing.Point(197, 8)
        Me.btCancel.Name = "btCancel"
        Me.btCancel.Size = New System.Drawing.Size(87, 29)
        Me.btCancel.TabIndex = 31
        Me.btCancel.Text = "Cancel"
        Me.btCancel.UseVisualStyleBackColor = False
        '
        'btDelete
        '
        Me.btDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btDelete.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btDelete.ForeColor = System.Drawing.Color.White
        Me.btDelete.Location = New System.Drawing.Point(197, 8)
        Me.btDelete.Name = "btDelete"
        Me.btDelete.Size = New System.Drawing.Size(86, 29)
        Me.btDelete.TabIndex = 30
        Me.btDelete.Text = "Delete"
        Me.btDelete.UseVisualStyleBackColor = False
        '
        'btEdit
        '
        Me.btEdit.BackColor = System.Drawing.Color.Orange
        Me.btEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btEdit.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEdit.ForeColor = System.Drawing.Color.White
        Me.btEdit.Location = New System.Drawing.Point(105, 8)
        Me.btEdit.Name = "btEdit"
        Me.btEdit.Size = New System.Drawing.Size(85, 29)
        Me.btEdit.TabIndex = 28
        Me.btEdit.Text = "Edit"
        Me.btEdit.UseVisualStyleBackColor = False
        '
        'btSave
        '
        Me.btSave.BackColor = System.Drawing.Color.DarkGreen
        Me.btSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btSave.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSave.ForeColor = System.Drawing.Color.White
        Me.btSave.Location = New System.Drawing.Point(105, 8)
        Me.btSave.Name = "btSave"
        Me.btSave.Size = New System.Drawing.Size(84, 29)
        Me.btSave.TabIndex = 29
        Me.btSave.Text = "Save"
        Me.btSave.UseVisualStyleBackColor = False
        '
        'btUpload
        '
        Me.btUpload.BackColor = System.Drawing.Color.FromArgb(CType(CType(225, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btUpload.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btUpload.Location = New System.Drawing.Point(288, 266)
        Me.btUpload.Name = "btUpload"
        Me.btUpload.Size = New System.Drawing.Size(133, 51)
        Me.btUpload.TabIndex = 13
        Me.btUpload.Text = "select image"
        Me.btUpload.UseVisualStyleBackColor = False
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(528, 222)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(240, 27)
        Me.txtPrice.TabIndex = 12
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(528, 186)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(240, 27)
        Me.txtName.TabIndex = 11
        '
        'txtCode
        '
        Me.txtCode.Location = New System.Drawing.Point(528, 145)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(240, 27)
        Me.txtCode.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(508, 222)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 21)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = ":"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(508, 186)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(14, 21)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = ":"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(508, 145)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(14, 21)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = ":"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(448, 222)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 21)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Price"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(448, 186)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 21)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(448, 145)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 21)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Code"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(439, 105)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(365, 24)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "NamaMakanan"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pictMenu1
        '
        Me.pictMenu1.Location = New System.Drawing.Point(288, 105)
        Me.pictMenu1.Name = "pictMenu1"
        Me.pictMenu1.Size = New System.Drawing.Size(145, 145)
        Me.pictMenu1.TabIndex = 2
        Me.pictMenu1.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'AdminMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 536)
        Me.Controls.Add(Me.PanelEmployee2)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "AdminMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.PanelLogout.ResumeLayout(False)
        Me.PanelPassword.ResumeLayout(False)
        Me.PanelMember.ResumeLayout(False)
        Me.PanelMenu.ResumeLayout(False)
        Me.PanelEmployee.ResumeLayout(False)
        Me.PanelHome.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelEmployee2.ResumeLayout(False)
        Me.PanelEmployee2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        CType(Me.pictMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents namaAdmin As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PanelEmployee2 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pictMenu1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtCode As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btUpload As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents btNew As System.Windows.Forms.Button
    Friend WithEvents btCancel As System.Windows.Forms.Button
    Friend WithEvents btDelete As System.Windows.Forms.Button
    Friend WithEvents btEdit As System.Windows.Forms.Button
    Friend WithEvents btSave As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents byCode As System.Windows.Forms.RadioButton
    Friend WithEvents byName As System.Windows.Forms.RadioButton
    Friend WithEvents QuerySearch As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents btPromo As Button
    Friend WithEvents PanelLogout As Panel
    Friend WithEvents btLogout As Button
    Friend WithEvents PanelPassword As Panel
    Friend WithEvents btAccount As Button
    Friend WithEvents PanelMember As Panel
    Friend WithEvents btMember As Button
    Friend WithEvents PanelMenu As Panel
    Friend WithEvents btMenu As Button
    Friend WithEvents PanelEmployee As Panel
    Friend WithEvents btEmployee As Button
    Friend WithEvents PanelHome As Panel
    Friend WithEvents btHome As Button
End Class
