﻿Imports MySql.Data.MySqlClient
Public Class AdminEmployee
    Dim iduser, role_id As Integer
    Dim role, path As String
    Private Sub fromAdmin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Employee()
        loadData()
        txtDisabled()
        btCancel.Visible = False
        autoComplateName()
        txtPositionShow()
    End Sub
    Sub loadData()
        Try
            'Call koneksi()
            'str = "select users.*, roles.name as role from users, roles where roles.id = users.role_id and users.role_id=3 order by users.name Asc"
            'cmd = New MySqlCommand(str, conn)
            'rd = cmd.ExecuteReader
            'rd.Read()
            'role = CStr(rd("role"))
            'If rd.HasRows Then
            Call koneksi()
            str = "select roles.name as role, users.name, users.email, users.hp, users.brithday,users.joindate ,users.photo from users, roles where roles.id = users.role_id and (role_id = 3 or role_id = 4) order by role asc"
            dt = New DataTable
            da = New MySql.Data.MySqlClient.MySqlDataAdapter(str, conn)
            DBData = New MySql.Data.MySqlClient.MySqlCommandBuilder(da) 'untuk bisa edit datagridview
            da.Fill(dt)
            DataGridView1.DataSource = dt

            DataGridView1.Columns(0).HeaderText = "Name"
            DataGridView1.Columns(1).HeaderText = "Email"
            DataGridView1.Columns(2).HeaderText = "Phone Number"
            DataGridView1.Columns(3).HeaderText = "Birthday"
            DataGridView1.Columns(4).HeaderText = "Join Date"
            DataGridView1.Columns(5).HeaderText = "Photo Location"

            'DGView(Properties)
            DataGridView1.DefaultCellStyle.Font = New Font("Candara", 14)
            DataGridView1.RowHeadersDefaultCellStyle.Font = New Font("Candara", 14, FontStyle.Bold)
            DataGridView1.ReadOnly = True
            DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders

            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub dataKlik()
        Try
            If DataGridView1.RowCount > 0 Then
                With DataGridView1
                    baris = .CurrentRow.Index
                    txtName.Text = CStr(.Item(0, baris).Value)
                    txtEmail.Text = CStr(.Item(1, baris).Value)
                    txtPhone.Text = CStr(.Item(2, baris).Value)
                    txtDate.Text = CStr(.Item(3, baris).Value)
                    path = CStr(.Item(5, baris).Value)
                    txtPosition.Text = CStr(.Item(6, baris).Value)
                    pict1.SizeMode = PictureBoxSizeMode.StretchImage
                    pict1.Image = Image.FromFile(path)
                    txtPhoto.Text = Path.Substring(Path.LastIndexOf("\") + 1)
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub Employee()
        btHome.ForeColor = Color.Silver
        btEmployee.ForeColor = Color.White
        btMenu.ForeColor = Color.Silver
        btAccount.ForeColor = Color.Silver
        btMember.ForeColor = Color.Silver
        btLogout.ForeColor = Color.Red
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Employee()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Sub txtDisabled()
        txtName.Enabled = False
        txtEmail.Enabled = False
        txtDate.Enabled = False
        txtPhone.Enabled = False
        txtPosition.Enabled = False
        txtPhoto.Enabled = False
        byName.Checked = True
        txtkosong()
        txtDate.Text = Now()
        btDelete.Enabled = True
        btSave.Enabled = False

    End Sub
    Sub txtEnabled()
        txtName.Enabled = True
        txtDate.Enabled = True
        txtPhone.Enabled = True
        txtPosition.Enabled = True
        btSave.Enabled = True
        btDelete.Enabled = False
    End Sub
    Sub txtkosong()
        txtPosition.Text = ""
        txtPhone.Text = ""
        txtPhone.Text = ""
        txtName.Text = ""
        txtEmail.Text = ""
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        dataKlik()
    End Sub

    'Private Sub btCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    txtDisabled()

    '    btEdit.Visible = True
    '    btCancel.Visible = False
    'End Sub

    'Private Sub btEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    txtEnabled()
    '    btEdit.Visible = False
    '    btCancel.Visible = True
    'End Sub
    Sub txtPositionShow()
        Try
            txtPosition.Items.Clear()
            Call koneksi()
            str = "select * from roles order by id asc"
            cmd = New MySqlCommand(str, conn)
            rd = cmd.ExecuteReader
            If rd.HasRows Then
                Do While rd.Read
                    txtPosition.Items.Add(rd("name"))
                Loop
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txtPosition_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPosition.Click
        txtPositionShow()
    End Sub
    Sub autoComplateName()
        Call koneksi()
        str = "select users.name from users where users.role_id=3 order by name asc"
        cmd = New MySqlCommand(str, conn)
        rd = cmd.ExecuteReader

        If rd.HasRows Then
            Do While rd.Read
                QuerySearch.AutoCompleteCustomSource.Add(CStr(rd("name")))
            Loop
        End If
    End Sub


    Private Sub btSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSave.Click
        Try
            Call koneksi()
            str = "select email from users where email = '" & txtEmail.Text & "'"
            cmd = New MySqlCommand(str, conn)
            rd = cmd.ExecuteReader
            If rd.HasRows Then
                Call koneksi()
                str = "update users set name = '" & txtName.Text & "', hp='" & txtPhone.Text & "', brithday = '" & Format(txtDate.Value, "yyyy-MM-dd") & "', photo = '" & Replace(path, "\", "\\") & "', role_id= '" & txtPosition.SelectedIndex + 1 & "' where email = '" & txtEmail.Text & "'"
                cmd = New MySqlCommand(str, conn)
                cmd.ExecuteNonQuery()
                loadData()
                MsgBox("Berhasil Mengubah Data Karyawan")
            Else
                Call koneksi()
                str = "insert into users (name,email,hp,brithday,joindate,photo,role_id) VALUES ('" & txtName.Text & "','" & txtEmail.Text & "', '" & txtPhone.Text & "','" & Format(txtDate.Value, "yyyy-MM-dd") & "','" & Format(Now(), "yyyy-MM-dd") & "','" & Replace(path, "\", "\\") & "', '" & txtPosition.SelectedIndex + 1 & "')"
                cmd = New MySqlCommand(str, conn)
                cmd.ExecuteNonQuery()
                loadData()
                MsgBox("Berhail Menambah Data Karyawan", vbInformation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        loadData()
    End Sub

    Private Sub btCancel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCancel.Click
        txtDisabled()
        btCancel.Visible = False
        btNew.Visible = True
        btEdit.Visible = True
        DataGridView1.Enabled = True
    End Sub

    Private Sub btEdit_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEdit.Click
        txtEnabled()
        btNew.Visible = False
        btEdit.Visible = False
        btCancel.Visible = True
        txtEmail.Enabled = False
        DataGridView1.Enabled = True
    End Sub

    Private Sub btNew_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btNew.Click
        txtEnabled()
        txtkosong()
        txtEmail.Enabled = True
        btNew.Visible = False
        DataGridView1.Enabled = False
        btEdit.Visible = False
        btCancel.Visible = True
    End Sub

    Private Sub btUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btUpload.Click
        On Error Resume Next
        OpenFileDialog1.Filter = "JPG FILES(*jpg)|*.jpg|JPEG FILEA(*jpeg)|*.jpeg|PNG FILES (*png)|*.png"
        OpenFileDialog1.FileName = ""
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            pict1.SizeMode = PictureBoxSizeMode.StretchImage
            pict1.Image = New Bitmap(OpenFileDialog1.FileName)
            path = OpenFileDialog1.FileName
            txtPhoto.Text = path.Substring(path.LastIndexOf("\") + 1)
            txtPhoto.Text = OpenFileDialog1.FileName.Trim
        End If
    End Sub

    Private Sub QuerySearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles QuerySearch.TextChanged
        Try
            If byName.Checked = True Then
                Call koneksi()
                str = "select roles.name as role, users.name, users.email, users.hp, users.brithday,users.joindate ,users.photo from users, roles where roles.id = users.role_id and (role_id = 3 or role_id = 4) and users.name like '%" & QuerySearch.Text & "%' order by name asc"
                cmd = New MySqlCommand(str, conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    Call koneksi()
                    da = New MySql.Data.MySqlClient.MySqlDataAdapter("select roles.name as role, users.name, users.email, users.hp, users.brithday,users.joindate ,users.photo from users, roles where roles.id = users.role_id and (role_id = 3 or role_id = 4) and users.name like '%" & QuerySearch.Text & "%' order by name asc", conn)
                    ds = New DataSet
                    da.Fill(ds, "ketemu")
                    DataGridView1.DataSource = ds.Tables("ketemu")
                    DataGridView1.ReadOnly = True
                Else
                    MsgBox("Tidak Di Temukan !", vbInformation)
                End If
            End If
            If byEmail.Checked = True Then
                Call koneksi()
                str = "select roles.name as role, users.name, users.email, users.hp, users.brithday,users.joindate ,users.photo from users, roles where roles.id = users.role_id and (role_id = 3 or role_id = 4) and users.email like '%" & QuerySearch.Text & "%' order by name asc"
                cmd = New MySqlCommand(str, conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    Call koneksi()
                    da = New MySql.Data.MySqlClient.MySqlDataAdapter("select roles.name as role, users.name, users.email, users.hp, users.brithday,users.joindate ,users.photo from users, roles where roles.id = users.role_id and (role_id = 3 or role_id = 4) and users.email like '%" & QuerySearch.Text & "%' order by name asc", conn)
                    ds = New DataSet
                    da.Fill(ds, "ketemu")
                    DataGridView1.DataSource = ds.Tables("ketemu")
                    DataGridView1.ReadOnly = True
                Else
                    MsgBox("Tidak Di Temukan !", vbInformation)
                End If
            End If
            If byAll.Checked = True Then
                Call koneksi()
                str = "select roles.name as role, users.name, users.email, users.hp, users.brithday,users.joindate ,users.photo from users, roles where roles.id = users.role_id and (role_id = 3 or role_id = 4) and users.* like '%" & QuerySearch.Text & "%' order by name asc"
                cmd = New MySqlCommand(str, conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows Then
                    Call koneksi()
                    da = New MySql.Data.MySqlClient.MySqlDataAdapter("select roles.name as role, users.name, users.email, users.hp, users.brithday,users.joindate ,users.photo from users, roles where roles.id = users.role_id and (role_id = 3 or role_id = 4) and users.* like '%" & QuerySearch.Text & "%' order by name asc", conn)
                    ds = New DataSet
                    da.Fill(ds, "ketemu")
                    DataGridView1.DataSource = ds.Tables("ketemu")
                    DataGridView1.ReadOnly = True
                Else
                    MsgBox("Tidak Di Temukan !", vbInformation)
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btDelete.Click
        Try
            Call koneksi()
            str = "delete from users where email = '" & txtEmail.Text & "'"
            cmd = New MySqlCommand(str, conn)
            rd = cmd.ExecuteReader
            If rd.HasRows Then
                cmd.ExecuteNonQuery()
                MsgBox("Berhasil Di Hapus !", vbInformation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AdminMenu.Show()
        Me.Hide()
    End Sub

    Private Sub btMember_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AdminMember.Show()
        Me.Hide()
    End Sub

    Private Sub btAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AdminAccount.Show()
        Me.Hide()
    End Sub
End Class
