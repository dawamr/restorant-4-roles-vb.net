﻿Imports MySql.Data.MySqlClient
Public Class AdminPromo
    Private Sub AdminPromo_Load(sender As Object, e As EventArgs) Handles Me.Load
        loadData()
        btNew.Visible = True
        btEdit.Visible = True
        btDelete.Visible = True
        btSave.Visible = False
        btCancel.Visible = False
    End Sub
    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Sub loadData()
        Try
            Call koneksi()
            str = "select * from promo order by status ASC"
            dt = New DataTable
            da = New MySql.Data.MySqlClient.MySqlDataAdapter(str, conn)
            DBData = New MySql.Data.MySqlClient.MySqlCommandBuilder(da) 'untuk bisa edit datagridview
            da.Fill(dt)
            DataGridView1.DataSource = dt

            DataGridView1.Columns(0).Visible = False
            DataGridView1.Columns(1).HeaderText = "Code"
            DataGridView1.Columns(2).HeaderText = "Name"
            DataGridView1.Columns(3).HeaderText = "Type"
            DataGridView1.Columns(4).HeaderText = "Promo"
            DataGridView1.Columns(5).HeaderText = "Satus"

            'DGView(Properties)
            DataGridView1.DefaultCellStyle.Font = New Font("Candara", 14)
            DataGridView1.RowHeadersDefaultCellStyle.Font = New Font("Candara", 14, FontStyle.Bold)
            DataGridView1.ReadOnly = True
            DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
        Catch ex As Exception

        End Try
    End Sub

    Sub dataKlik()
        Try
            If DataGridView1.RowCount > 0 Then
                With DataGridView1
                    baris = .CurrentRow.Index
                    idPromo = CInt(.Item(0, baris).Value)
                    txtCode.Text = CStr(.Item(1, baris).Value)
                    txtName.Text = CStr(.Item(2, baris).Value)
                    txtType.Text = CStr(.Item(3, baris).Value)
                    txtPromo.Text = CStr(.Item(4, baris).Value)
                    txtStatus.Text = CStr(.Item(5, baris).Value)
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Sub txtKosong()
        txtCode.Text = ""
        txtName.Text = ""
        txtPromo.Text = ""
        txtStatus.Text = ""
        txtType.Text = ""
    End Sub
    Sub txtdisabled()
        txtCode.Enabled = False
        txtName.Enabled = False
        txtPromo.Enabled = False
        txtStatus.Enabled = False
        txtType.Enabled = False
    End Sub
    Sub txtenabled()
        txtCode.Enabled = True
        txtName.Enabled = True
        txtPromo.Enabled = True
        txtStatus.Enabled = True
        txtType.Enabled = True
    End Sub

    Private Sub btNew_Click(sender As Object, e As EventArgs) Handles btNew.Click
        DataGridView1.Enabled = False
        btNew.Visible = False
        btEdit.Visible = False
        btDelete.Visible = False
        btSave.Visible = True
        btCancel.Visible = True
        txtenabled()
        txtKosong()
        loadData()
    End Sub

    Private Sub btEdit_Click(sender As Object, e As EventArgs) Handles btEdit.Click
        txtenabled()
        btNew.Visible = False
        btEdit.Visible = False
        btDelete.Visible = False
        btSave.Visible = True
        btCancel.Visible = True
        DataGridView1.Enabled = True
    End Sub

    Private Sub btCancel_Click(sender As Object, e As EventArgs) Handles btCancel.Click
        DataGridView1.Enabled = True
        btNew.Visible = True
        btEdit.Visible = True
        btDelete.Visible = True
        btSave.Visible = False
        btCancel.Visible = False
        txtdisabled()
        txtKosong()
        loadData()
    End Sub

    Private Sub btSave_Click(sender As Object, e As EventArgs) Handles btSave.Click
        codePromo = txtCode.Text
        namePromo = txtName.Text
        TypePromo = txtType.Text
        promo = txtPromo.Text
        statusPromo = txtStatus.Text
        cekPromo()
        If rd.HasRows Then
            UpdatePromo()
        Else
            AddPromo()
        End If
        loadData()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        dataKlik()
    End Sub

    Private Sub QuerySearch_TextChanged(sender As Object, e As EventArgs) Handles QuerySearch.TextChanged
        SearchQuery = QuerySearch.Text
        If SearchQuery = "" Then
                loadData()
            End If
            If byCode.Checked = True Then
                LikeCodePromo()
                DataGridView1.DataSource = ds.Tables("ketemu")
                DataGridView1.ReadOnly = True
            End If
            If byName.Checked = True Then
                LikeNamePromo()
                DataGridView1.DataSource = ds.Tables("ketemu")
                DataGridView1.ReadOnly = True
            End If

    End Sub

    Private Sub btDelete_Click(sender As Object, e As EventArgs) Handles btDelete.Click
        'MsgBox("Yakin Hapus Promo " + txtCode.Text, vbOKCancel)
        'If vbOK = True Then
        DeletePromo()
        ' End If
        'If MsgBoxResult.Cancel = truw Then
        '    Application.Exit()
        'End If
    End Sub
End Class
