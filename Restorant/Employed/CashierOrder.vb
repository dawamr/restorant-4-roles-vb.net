﻿Imports MySql.Data.MySqlClient
Public Class CashierOrder
    Dim codeMakanan As String
    Dim table As New DataTable("table")
    Dim index, index2, harga As Integer
    Public totalPay As Integer = 0
    Dim RowSelected As DataGridViewRow
    Private Sub CashierPayment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        formLoad()
    End Sub
    Sub formLoad()
        PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        PictureBox1.Image = Image.FromFile(photoUser)
        namaAdmin.Text = nameUser
        loadData()
        data2load()
        lblTotalPrice.Text = 0
        txtDisabled()
        kodeTransaksi()
        deleteDatagrid()
    End Sub
    Sub deleteDatagrid()
        Try
            Dim y As Integer = DataGridView2.Rows.Count
            For barisAtas As Integer = 0 To y - 1
                'MsgBox(DataGridView2.Rows(barisAtas).Cells(1).Value)
                DataGridView2.Rows.RemoveAt(0)
            Next

        Catch ex As Exception
            'MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub kodeTransaksi()
        txtCode.Text = "REST-NT-" + Format(Date.Now(), "yyyyMMdd-hhmm-ss")
    End Sub
    Sub txtDisabled()
        txtCode.Enabled = False
        txtName.Enabled = False
        txtPrice.Enabled = False
        txtName.Text = ""
        txtPrice.Text = ""
    End Sub
    Private Sub btNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        btPay.Visible = True
        btAdd.Visible = True
    End Sub
    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Sub loadData()
        Try
            Call koneksi()
            str = "select * from menu order by name asc"
            dt = New DataTable
            da = New MySql.Data.MySqlClient.MySqlDataAdapter(str, conn)
            DBData = New MySql.Data.MySqlClient.MySqlCommandBuilder(da) 'untuk bisa edit datagridview
            da.Fill(dt)
            DataGridView1.DataSource = dt

            DataGridView1.Columns(0).Visible = False
            DataGridView1.Columns(1).Visible = False
            DataGridView1.Columns(2).HeaderText = "Menu"
            DataGridView1.Columns(3).HeaderText = "Harga"
            DataGridView1.Columns(4).Visible = False

            'DGView(Properties)
            DataGridView1.DefaultCellStyle.Font = New Font("Candara", 14)
            DataGridView1.RowHeadersDefaultCellStyle.Font = New Font("Candara", 14, FontStyle.Bold)
            DataGridView1.ReadOnly = True
            DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub dataKlik()
        Try
            txtDisabled()

            If DataGridView1.RowCount > 0 Then
                With DataGridView1
                    baris = .CurrentRow.Index
                    idMenu = CInt(.Item(0, baris).Value)
                    codeMakanan = CStr(.Item(1, baris).Value)
                    txtName.Text = CStr(.Item(2, baris).Value)
                    txtPrice.Text = CStr(.Item(3, baris).Value)
                    txtQty.Text = 1
                    Call koneksi()
                    str = "select photo from menu where code = '" & CStr(.Item(1, baris).Value) & "' limit 1"
                    cmd = New MySqlCommand(str, conn)
                    rd = cmd.ExecuteReader
                    rd.Read()
                    If rd.HasRows Then
                        path = CStr(rd("photo"))
                        pictMenu1.SizeMode = PictureBoxSizeMode.StretchImage
                        pictMenu1.Image = Image.FromFile(path)
                    End If
                End With
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        dataKlik()
    End Sub
    Sub data2load()
        Try
            table.Columns.Add("MenuId", Type.GetType("System.Int32"))
            ' DataGridView2.Columns(0).Visible = False
            table.Columns.Add("Nama", Type.GetType("System.String"))
            table.Columns.Add("Price", Type.GetType("System.Int32"))
            table.Columns.Add("Qty", Type.GetType("System.Int32"))

            DataGridView2.DataSource = table
        Catch ex As Exception

        End Try
    End Sub
    Private Sub btAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btAdd.Click
        Dim y As Integer = DataGridView2.Rows.Count
        Dim x As Integer = DataGridView2.Rows.Count - 1
        Try
            'For intI As Integer = 0 To y
            '    For intJ As Integer = intI + 1 To x
            '        If DataGridView2.Rows(intI).Cells(0).Value = DataGridView2.Rows(intJ).Cells(0).Value Then
            '            MsgBox("Duplicate: " & DataGridView2.Rows(intJ).Cells(0).Value)
            '        End If
            '        Exit Sub
            '    Next
            'Next
            For barisatas As Integer = 1 To y
                For barisbawah = barisatas + 1 To x
                    If DataGridView2.Rows(barisbawah).Cells(0).Value = DataGridView2.Rows(barisatas).Cells(0).Value Then
                        DataGridView2.Rows(barisbawah).Cells(2).Value = txtQty.Text
                        MsgBox("Duplicate: " & DataGridView2.Rows(barisbawah).Cells(0).Value)
                        'DataGridView2.Rows.RemoveAt(DataGridView2.CurrentCell.RowIndex)
                        Exit Sub
                    End If
                Next
            Next
            table.Rows.Add(idMenu, txtName.Text, txtPrice.Text, txtQty.Text)
            DataGridView2.DataSource = table
            loadData()
            lblTotalPrice.Text = CInt(lblTotalPrice.Text) + (CInt(txtPrice.Text) * CInt(txtQty.Text))
            SendKeys.Send("(down)")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDelete.Click
        Try
            MsgBox("Hapus Item " + DataGridView2.Rows(index2).Cells(1).Value, vbOKCancel)
            If vbOK Then
                lblTotalPrice.Text = CInt(lblTotalPrice.Text) - DataGridView2.Rows(index2).Cells(2).Value
                DataGridView2.Rows.RemoveAt(index2)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView2_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        index2 = e.RowIndex
        RowSelected = DataGridView2.Rows(index2)
        harga = RowSelected.Cells(2).Value
        txtName.Text = RowSelected.Cells(1).Value.ToString
        txtPrice.Text = harga
        txtQty.Text = CStr(RowSelected.Cells(3).Value)
    End Sub

    Private Sub btPay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btPay.Click
        Metode.Show()
    End Sub

    Private Sub QuerySearch_TextChanged(sender As Object, e As EventArgs) Handles QuerySearch.TextChanged
        If QuerySearch.Text.Length > 1 Then
            loadData()
        End If
        If byName.Checked = True Then
            SearchQuery = QuerySearch.Text
            Try
                LikeNameFood()
                DataGridView1.DataSource = ds.Tables("ketemu")
                DataGridView1.Columns(0).Visible = False
                DataGridView1.Columns(1).Visible = False
                DataGridView1.Columns(4).Visible = False
                DataGridView1.ReadOnly = True
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        If byPrice.Checked = True Then
            SearchQuery = QuerySearch.Text
            If QuerySearch.Text.Length > 4 Then
                Try
                    LikePriceLowersFood()
                    DataGridView1.DataSource = ds.Tables("ketemu")
                    DataGridView1.Columns(0).Visible = False
                    DataGridView1.Columns(1).Visible = False
                    DataGridView1.Columns(4).Visible = False
                    DataGridView1.ReadOnly = True
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        End If
    End Sub
End Class
