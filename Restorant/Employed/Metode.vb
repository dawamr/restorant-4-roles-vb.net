﻿Public Class Metode
    Friend payType As Integer = 1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            payType = 0
            CashierPayment.loadData()
            CashierPayment.Show()
            Me.Hide()
            CashierOrder.Hide()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            payType = 1
            CashierPayment.loadData()
            CashierPayment.txtKembalian.Text = 0
            CashierPayment.Show()
            Me.Hide()
            CashierOrder.Hide()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class