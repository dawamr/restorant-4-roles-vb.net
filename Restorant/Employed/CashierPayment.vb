﻿Imports MySql.Data.MySqlClient
Public Class CashierPayment
    Dim codeMakanan As String
    Dim table As New DataTable("table")
    Dim index As Integer
    Public totalPay As Integer = 0
    Dim RowSelected As DataGridViewRow
    Private Sub CashierPayment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        loadData()
    End Sub

    Sub loadData()
        namaAdmin.Text = nameUser
        pict1.SizeMode = PictureBoxSizeMode.StretchImage
        pict1.Image = Image.FromFile(photoUser)
        txtRex.Text = CashierOrder.txtCode.Text
        lblTotal.Text = CashierOrder.lblTotalPrice.Text
        DataGridView2.DataSource = CashierOrder.DataGridView2.DataSource
        txtDibayar.Text = 0
        If Metode.payType = 0 Then
            txtType.Text = "Credit"
            k2and1.Visible = False
            k2And2.Visible = False
            txtDibayar.Visible = False
            txtKembalian.Visible = False
            K1and1.Visible = True
            K1and2.Visible = True
            txtNumber.Visible = True
            txtBank.Visible = True

        End If
        If Metode.payType = 1 Then
            txtType.Text = "Cash"
            K1and1.Visible = False
            K1and2.Visible = False
            txtNumber.Visible = False
            txtBank.Visible = False
            k2and1.Visible = True
            k2And2.Visible = True
            txtDibayar.Visible = True
            txtKembalian.Visible = True
        End If
    End Sub
    Private Sub btNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        btPay.Visible = True
    End Sub
    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub ComboBox1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtDibayar_TextChanged(sender As Object, e As EventArgs) Handles txtDibayar.TextChanged
        Try
            txtKembalian.Text = CInt(txtDibayar.Text) - CInt(lblTotal.Text)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btPay_Click(sender As Object, e As EventArgs) Handles btPay.Click
        Try
            Dim harga As Integer
            idMember = vbNull
            idPromo = vbNull
            idUser = LoginRegister.idLogin
            typePayments = txtType.Text
            totalPayments = lblTotal.Text
            codePayments = txtRex.Text
            NoRekening = txtNumber.Text
            Rekening = txtBank.Text
            bayarPayments = txtDibayar.Text
            kembaliPayments = txtKembalian.Text
            PaymentsOrder()
            If rd.HasRows Then
                rd.Read()
                idOrder = rd("id")
                Dim y As Integer = DataGridView2.Rows.Count
                For barisatas As Integer = 0 To y - 1
                    idMenu = DataGridView2.Rows(barisatas).Cells(0).Value
                    qtyOrders = DataGridView2.Rows(barisatas).Cells(3).Value
                    loadMenuWereId()
                    If rd.HasRows Then
                        rd.Read()
                        harga = rd("harga")
                    End If
                    priceOrders = qtyOrders * harga
                    messegeOrders = "Nothing"
                    InsertDetailOrder()
                Next
                MsgBox("Berhasil Melakukan Transaksi !", vbInformation)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btCancel_Click(sender As Object, e As EventArgs) Handles btCancel.Click
        CashierOrder.formLoad()
        CashierOrder.Show()
        Me.Hide()
    End Sub

    Private Sub DataGridView2_Click(sender As Object, e As EventArgs) Handles DataGridView2.Click
        loadData()
    End Sub
End Class
