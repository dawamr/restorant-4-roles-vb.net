﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CashierPayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PanelLogout = New System.Windows.Forms.Panel()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.PanelPassword = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.PanelPayment = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PanelHome = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.namaAdmin = New System.Windows.Forms.Label()
        Me.pict1 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanelHome2 = New System.Windows.Forms.Panel()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtType = New System.Windows.Forms.Label()
        Me.txtDibayar = New System.Windows.Forms.TextBox()
        Me.txtKembalian = New System.Windows.Forms.TextBox()
        Me.k2And2 = New System.Windows.Forms.Label()
        Me.k2and1 = New System.Windows.Forms.Label()
        Me.txtRex = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.K1and2 = New System.Windows.Forms.Label()
        Me.K1and1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBank = New System.Windows.Forms.ComboBox()
        Me.txtNumber = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.btCancel = New System.Windows.Forms.Button()
        Me.btPay = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.PanelLogout.SuspendLayout()
        Me.PanelPassword.SuspendLayout()
        Me.PanelPayment.SuspendLayout()
        Me.PanelHome.SuspendLayout()
        CType(Me.pict1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelHome2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel9.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Panel1.Controls.Add(Me.PanelLogout)
        Me.Panel1.Controls.Add(Me.PanelPassword)
        Me.Panel1.Controls.Add(Me.PanelPayment)
        Me.Panel1.Controls.Add(Me.PanelHome)
        Me.Panel1.Controls.Add(Me.namaAdmin)
        Me.Panel1.Controls.Add(Me.pict1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.ForeColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(207, 536)
        Me.Panel1.TabIndex = 0
        '
        'PanelLogout
        '
        Me.PanelLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelLogout.Controls.Add(Me.Button4)
        Me.PanelLogout.Location = New System.Drawing.Point(0, 316)
        Me.PanelLogout.Name = "PanelLogout"
        Me.PanelLogout.Size = New System.Drawing.Size(207, 39)
        Me.PanelLogout.TabIndex = 7
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button4.FlatAppearance.BorderSize = 0
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(4, 0)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(203, 39)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Logout"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.UseVisualStyleBackColor = False
        '
        'PanelPassword
        '
        Me.PanelPassword.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelPassword.Controls.Add(Me.Button5)
        Me.PanelPassword.Location = New System.Drawing.Point(0, 271)
        Me.PanelPassword.Name = "PanelPassword"
        Me.PanelPassword.Size = New System.Drawing.Size(207, 39)
        Me.PanelPassword.TabIndex = 6
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button5.FlatAppearance.BorderSize = 0
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(4, 0)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(203, 39)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "Change Password"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.UseVisualStyleBackColor = False
        '
        'PanelPayment
        '
        Me.PanelPayment.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(245, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.PanelPayment.Controls.Add(Me.Button2)
        Me.PanelPayment.Location = New System.Drawing.Point(0, 226)
        Me.PanelPayment.Name = "PanelPayment"
        Me.PanelPayment.Size = New System.Drawing.Size(207, 39)
        Me.PanelPayment.TabIndex = 3
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(4, 0)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(203, 39)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Payments"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = False
        '
        'PanelHome
        '
        Me.PanelHome.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(110, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.PanelHome.Controls.Add(Me.Button1)
        Me.PanelHome.Location = New System.Drawing.Point(0, 181)
        Me.PanelHome.Name = "PanelHome"
        Me.PanelHome.Size = New System.Drawing.Size(207, 39)
        Me.PanelHome.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(80, Byte), Integer))
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(4, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(203, 39)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Home"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = False
        '
        'namaAdmin
        '
        Me.namaAdmin.Font = New System.Drawing.Font("Century Gothic", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.namaAdmin.Location = New System.Drawing.Point(0, 117)
        Me.namaAdmin.Name = "namaAdmin"
        Me.namaAdmin.Size = New System.Drawing.Size(207, 25)
        Me.namaAdmin.TabIndex = 1
        Me.namaAdmin.Text = "Nama"
        Me.namaAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pict1
        '
        Me.pict1.Image = Global.Restorant.My.Resources.Resources.boy_8
        Me.pict1.Location = New System.Drawing.Point(58, 24)
        Me.pict1.Name = "pict1"
        Me.pict1.Size = New System.Drawing.Size(90, 90)
        Me.pict1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pict1.TabIndex = 0
        Me.pict1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(15, Byte), Integer), CType(CType(25, Byte), Integer), CType(CType(125, Byte), Integer))
        Me.Panel2.Controls.Add(Me.PictureBox4)
        Me.Panel2.Controls.Add(Me.PictureBox3)
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(207, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(810, 15)
        Me.Panel2.TabIndex = 1
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.Restorant.My.Resources.Resources.transparent_button_yellow_3
        Me.PictureBox4.Location = New System.Drawing.Point(774, 2)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 2
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.Restorant.My.Resources.Resources.green_dog_bone_png
        Me.PictureBox3.Location = New System.Drawing.Point(756, 2)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 1
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.Restorant.My.Resources.Resources.button_svg_2
        Me.PictureBox2.Location = New System.Drawing.Point(792, 2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(334, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(142, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Form Payment"
        '
        'PanelHome2
        '
        Me.PanelHome2.AutoScroll = True
        Me.PanelHome2.BackColor = System.Drawing.Color.White
        Me.PanelHome2.Controls.Add(Me.lblTotal)
        Me.PanelHome2.Controls.Add(Me.Label6)
        Me.PanelHome2.Controls.Add(Me.Panel4)
        Me.PanelHome2.Controls.Add(Me.Label2)
        Me.PanelHome2.Controls.Add(Me.DataGridView2)
        Me.PanelHome2.Controls.Add(Me.Panel9)
        Me.PanelHome2.Controls.Add(Me.Panel3)
        Me.PanelHome2.Controls.Add(Me.Label1)
        Me.PanelHome2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelHome2.Location = New System.Drawing.Point(207, 15)
        Me.PanelHome2.Name = "PanelHome2"
        Me.PanelHome2.Size = New System.Drawing.Size(810, 521)
        Me.PanelHome2.TabIndex = 3
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.ForeColor = System.Drawing.Color.Red
        Me.lblTotal.Location = New System.Drawing.Point(108, 331)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(247, 24)
        Me.lblTotal.TabIndex = 82
        Me.lblTotal.Text = "0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 331)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(89, 24)
        Me.Label6.TabIndex = 81
        Me.Label6.Text = "Total"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtType)
        Me.Panel4.Controls.Add(Me.txtDibayar)
        Me.Panel4.Controls.Add(Me.txtKembalian)
        Me.Panel4.Controls.Add(Me.k2And2)
        Me.Panel4.Controls.Add(Me.k2and1)
        Me.Panel4.Controls.Add(Me.txtRex)
        Me.Panel4.Controls.Add(Me.Label8)
        Me.Panel4.Controls.Add(Me.K1and2)
        Me.Panel4.Controls.Add(Me.K1and1)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.txtBank)
        Me.Panel4.Controls.Add(Me.txtNumber)
        Me.Panel4.Location = New System.Drawing.Point(389, 85)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(397, 226)
        Me.Panel4.TabIndex = 80
        '
        'txtType
        '
        Me.txtType.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtType.Location = New System.Drawing.Point(134, 65)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(251, 21)
        Me.txtType.TabIndex = 14
        '
        'txtDibayar
        '
        Me.txtDibayar.Location = New System.Drawing.Point(134, 111)
        Me.txtDibayar.Name = "txtDibayar"
        Me.txtDibayar.Size = New System.Drawing.Size(247, 27)
        Me.txtDibayar.TabIndex = 13
        '
        'txtKembalian
        '
        Me.txtKembalian.Enabled = False
        Me.txtKembalian.Location = New System.Drawing.Point(134, 154)
        Me.txtKembalian.Name = "txtKembalian"
        Me.txtKembalian.Size = New System.Drawing.Size(247, 27)
        Me.txtKembalian.TabIndex = 12
        '
        'k2And2
        '
        Me.k2And2.AutoSize = True
        Me.k2And2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.k2And2.Location = New System.Drawing.Point(7, 154)
        Me.k2And2.Name = "k2And2"
        Me.k2And2.Size = New System.Drawing.Size(93, 21)
        Me.k2And2.TabIndex = 11
        Me.k2And2.Text = "Kembalian"
        '
        'k2and1
        '
        Me.k2and1.AutoSize = True
        Me.k2and1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.k2and1.Location = New System.Drawing.Point(7, 114)
        Me.k2and1.Name = "k2and1"
        Me.k2and1.Size = New System.Drawing.Size(73, 21)
        Me.k2and1.TabIndex = 10
        Me.k2and1.Text = "Di Bayar"
        '
        'txtRex
        '
        Me.txtRex.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRex.Location = New System.Drawing.Point(134, 27)
        Me.txtRex.Name = "txtRex"
        Me.txtRex.Size = New System.Drawing.Size(251, 21)
        Me.txtRex.TabIndex = 9
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(7, 27)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 21)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "CODE"
        '
        'K1and2
        '
        Me.K1and2.AutoSize = True
        Me.K1and2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.K1and2.Location = New System.Drawing.Point(7, 154)
        Me.K1and2.Name = "K1and2"
        Me.K1and2.Size = New System.Drawing.Size(100, 21)
        Me.K1and2.TabIndex = 7
        Me.K1and2.Text = "Bank Name"
        '
        'K1and1
        '
        Me.K1and1.AutoSize = True
        Me.K1and1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.K1and1.Location = New System.Drawing.Point(7, 111)
        Me.K1and1.Name = "K1and1"
        Me.K1and1.Size = New System.Drawing.Size(117, 21)
        Me.K1and1.TabIndex = 6
        Me.K1and1.Text = "Card Number"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 21)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Payment Type"
        '
        'txtBank
        '
        Me.txtBank.FormattingEnabled = True
        Me.txtBank.Items.AddRange(New Object() {"BNI", "BRI", "BCA"})
        Me.txtBank.Location = New System.Drawing.Point(134, 153)
        Me.txtBank.Name = "txtBank"
        Me.txtBank.Size = New System.Drawing.Size(256, 29)
        Me.txtBank.TabIndex = 2
        '
        'txtNumber
        '
        Me.txtNumber.Location = New System.Drawing.Point(134, 111)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(247, 27)
        Me.txtNumber.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(348, 24)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Payment"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.GridColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.DataGridView2.Location = New System.Drawing.Point(17, 85)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.Size = New System.Drawing.Size(350, 226)
        Me.DataGridView2.TabIndex = 78
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.btCancel)
        Me.Panel9.Controls.Add(Me.btPay)
        Me.Panel9.Location = New System.Drawing.Point(389, 331)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(397, 45)
        Me.Panel9.TabIndex = 77
        '
        'btCancel
        '
        Me.btCancel.BackColor = System.Drawing.Color.Orange
        Me.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btCancel.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btCancel.ForeColor = System.Drawing.Color.White
        Me.btCancel.Location = New System.Drawing.Point(287, 8)
        Me.btCancel.Name = "btCancel"
        Me.btCancel.Size = New System.Drawing.Size(98, 29)
        Me.btCancel.TabIndex = 30
        Me.btCancel.Text = "Cancel"
        Me.btCancel.UseVisualStyleBackColor = False
        '
        'btPay
        '
        Me.btPay.BackColor = System.Drawing.Color.DarkGreen
        Me.btPay.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btPay.Font = New System.Drawing.Font("Century Gothic", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btPay.ForeColor = System.Drawing.Color.White
        Me.btPay.Location = New System.Drawing.Point(138, 8)
        Me.btPay.Name = "btPay"
        Me.btPay.Size = New System.Drawing.Size(143, 29)
        Me.btPay.TabIndex = 29
        Me.btPay.Text = "Payment"
        Me.btPay.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 488)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(810, 33)
        Me.Panel3.TabIndex = 74
        '
        'CashierPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 536)
        Me.Controls.Add(Me.PanelHome2)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "CashierPayment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.PanelLogout.ResumeLayout(False)
        Me.PanelPassword.ResumeLayout(False)
        Me.PanelPayment.ResumeLayout(False)
        Me.PanelHome.ResumeLayout(False)
        CType(Me.pict1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelHome2.ResumeLayout(False)
        Me.PanelHome2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel9.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents namaAdmin As System.Windows.Forms.Label
    Friend WithEvents pict1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents PanelLogout As System.Windows.Forms.Panel
    Friend WithEvents PanelPassword As System.Windows.Forms.Panel
    Friend WithEvents PanelPayment As System.Windows.Forms.Panel
    Friend WithEvents PanelHome As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanelHome2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents txtNumber As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents DataGridView2 As DataGridView
    Friend WithEvents btPay As Button
    Friend WithEvents btCancel As Button
    Friend WithEvents K1and2 As Label
    Friend WithEvents K1and1 As Label
    Friend WithEvents txtBank As ComboBox
    Friend WithEvents lblTotal As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtRex As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtDibayar As TextBox
    Friend WithEvents txtKembalian As TextBox
    Friend WithEvents k2And2 As Label
    Friend WithEvents k2and1 As Label
    Friend WithEvents txtType As Label
End Class
